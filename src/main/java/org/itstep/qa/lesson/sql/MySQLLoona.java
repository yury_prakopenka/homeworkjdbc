package org.itstep.qa.lesson.sql;

import java.sql.*;
import java.util.Scanner;

public class MySQLLoona {
    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement prestat = null;
        Statement stat = null;
        int i = 0, j = 0;

        String queryUpdateAttrType = "INSERT INTO type_attr(type_attr_name) VALUES (?)";
        String queryUpdateAttr = "INSERT INTO attr (attr_name, quantity, type_attr_id) VALUES (?, ?, ?)";
        String queryCountAllFromAttr = "SELECT COUNT(*) FROM attr;";
        String queryGetFastAttr = "SELECT attr.id, attr.attr_name, attr.quantity, type_attr.type_attr_name\n" +
                "FROM type_attr\n" +
                "INNER JOIN attr\n" +
                "ON type_attr.id = attr.type_attr_id WHERE type_attr.type_attr_name = \'Передвижной\';";
        String queryGetQuantityAttr = "SELECT attr.id, attr.attr_name, attr.quantity, type_attr.type_attr_name\n" +
                "FROM type_attr\n" +
                "INNER JOIN attr\n" +
                "ON type_attr.id = attr.type_attr_id WHERE attr.quantity > 33;";
        String querySummaryQuantity = "SELECT SUM(quantity), type_attr.type_attr_name  from type_attr \n" +
                "INNER JOIN attr\n" +
                "ON type_attr.id = attr.type_attr_id GROUP BY type_attr.type_attr_name;";

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/loonapark", "root", "root"); //tckb

            prestat = connection.prepareStatement(queryUpdateAttrType);
            Scanner scanner = new Scanner(System.in);

            System.out.println("Заполните справочную таблицу");
            while (i != 2) {
                System.out.println("Введите тип аттракциона");
                prestat.setString(1, scanner.nextLine());
                prestat.executeUpdate();
                i++;
            }

            prestat = connection.prepareStatement(queryUpdateAttr);

            System.out.println("Заполните оперативную таблицу");
            while (j != 4) {
                System.out.println("Введите название аттракциона");
                prestat.setString(1, scanner.nextLine());
                System.out.println("Введите количество мест");
                prestat.setInt(2, scanner.nextInt());
                System.out.println("Выберите тип аттракциона (внешний ключ из справочной таблицы, цифра)");
                prestat.setInt(3, scanner.nextInt());
                scanner.nextLine();
                prestat.executeUpdate();
                j++;
            }
            System.out.println("-------------------------------------------------------------");
            stat = connection.createStatement();
            ResultSet rc = stat.executeQuery(queryCountAllFromAttr);

            System.out.println("Количество всех аттракционов");
            while (rc.next()) System.out.println(rc.getString(1));

            stat = connection.createStatement();
            rc = stat.executeQuery(queryGetFastAttr);

            System.out.println();
            System.out.println("Быстрые аттракционы");
            while (rc.next()) {
                System.out.println(rc.getString("attr.id") + " | " +
                                   rc.getString("attr.attr_name") + " | " +
                                   rc.getString("attr.quantity") + " | " +
                                   rc.getString("type_attr.type_attr_name"));
            }

            stat = connection.createStatement();
            rc = stat.executeQuery(queryGetQuantityAttr);

            System.out.println();
            System.out.println("Аттракционы с посадочными местами больше 35");
            while (rc.next()) {
                System.out.println(rc.getString("attr.id") + " | " +
                        rc.getString("attr.attr_name") + " | " +
                        rc.getString("attr.quantity") + " | " +
                        rc.getString("type_attr.type_attr_name"));
            }

            stat = connection.createStatement();
            rc = stat.executeQuery(querySummaryQuantity);

            System.out.println();
            System.out.println("Суммарное количество мест у передвижных и стационарных");
            while (rc.next()) System.out.println(rc.getString(1) + " | " + rc.getString(2));

            prestat.close();
            stat.close();
            connection.close();
        } catch (SQLException ex) {
            System.out.println("Ошибка при работе с БД");
            ex.printStackTrace();
        }


    }
}
